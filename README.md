# Pyspark Stroke Classification Project

This project is a redo of a previous [classification project](https://gitlab.com/MattySanchez20/stroke_classification) that used SKlearn to run the model. This project utilises MLlib from Spark.

## Objectives
- Clean stroke data using SKlearn preprocessing libraries
- Preprocess features to avoid model bias
- Convert categorical values into dummy variables
- Convert DataFrames into SparkDataFrames
- Use MLlib to run a classification model to predict stroke

## Results
Created a Random Forest Classification Model that has an accuracy of 75 percent.
